package tourGuide.model.user.request;


import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class AttractionDistance {
    private String attractionName;
    private Double longitude;
    private Double latitude;
    private Double distance;
    private Integer rewardPoints;
}
