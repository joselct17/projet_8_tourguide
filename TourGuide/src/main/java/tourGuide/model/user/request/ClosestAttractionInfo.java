package tourGuide.model.user.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import gpsUtil.location.Location;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ClosestAttractionInfo {

    public String name;
    public Location attractionLocation;
    public Location userLocation;
    public double distance;
    public int rewardPoints;

}
